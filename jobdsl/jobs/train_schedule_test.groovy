multibranchPipelineJob('train-schedule-multibranch') {
    branchSources {
        git{
            id('train-schedule-3235235235235325')
            remote('https://github.com/woodsiaceae/cicd-pipeline-train-schedule-git')
            credentialsId('Jenkins-git-ssh')
        }
    }
}